var ruffleTime = 10000; // 10 seconds
var topSpeed = 100; //0.05 second
var lowSpeed = 200; //0.01 second
var lastPhaseTime = ruffleTime * 30 / 100;
var switchPhase = ((ruffleTime / topSpeed) * 30 / 100); //0.01 second
var apiUrl = "";
var congUrl = "";
var ruffling = false;

let pagesFlow = ["home",  "ruffle", "coupon"];
var pageIdx = 0;
let foods = ["ayam", "bakso", "kopi", "martabak", "nasi"];
// let baseUrl = location.protocol + '//' + location.host + location.pathname;
let baseUrl = "";
var dataRuffle = null;
var modalCoupon = null; 

var startSrc = baseUrl+'assets/sounds/roll.wav';
var tadaSrc = baseUrl+'assets/sounds/ding.wav';
var tadaGrandSrc = baseUrl+'assets/sounds/tada.flac';

var startSound = new Howl({
    src: [startSrc],
    loop: true
  });
var tadaSound = new Howl({
    src: [tadaSrc]
  });
var tadaGrandSound = new Howl({
    src: [tadaGrandSrc]
  });

let orderNum = "'orderNumber'";
let voucher = "";

$(function() {
    changePage(0);
    $(document).on('click', '#start', function(ev){
        changePage(1);
    });
    dataRuffle = initData();
    // console.log(dataRuffle);
    document.body.onkeyup = function(e){
        if(e.keyCode == 32 || e.keyCode == 13) {
            if(ruffling) {
                return;
            }
            changePage(pageIdx);
         } 
    }


});

var initData = function() {
    var data = [];
    for(i = 0; i < foods.length; i++) {
        let foodType = foods[i];
        for(j = 0; j < 5; j++) {
            let item = {
                type : foodType,
                modal_id :"#modal-"+foodType+"",
                img : baseUrl+"assets/image/foods/"+foodType+"/"+(j+1)+".jpg"
            }
            data.push(item);
        }
    }
    return data;
}

var changePage = function(page) {
    switch (page) {
        case 0:  {
            $("#body").attr("class", "bg-1");
            $("#body").load("pages/home.html");
            pageIdx = page + 1;
            break;
        }
        case 1:  {
            $("#body").attr("class", "");
            $("#body").load("pages/ruffle.html", function() {
                dataRuffle = shuffle(dataRuffle);
                var elem = $("#imageRuffle");
                if(dataRuffle !== null && ruffling === false){
                    startSound.play();
                    startRuffle(dataRuffle, ruffleTime, topSpeed, elem);
                }
            });
            pageIdx = page + 1; 
            break;
        }
        case 2:  {
            $("#body").attr("class", "");
            $("#body").load("pages/voucher.html", function() {
                $("#voucher").text(voucher);
            });
            pageIdx = 0;
            break;
        }
    }

}


var startRuffle = function startRuffle(ruffles, totalTime, speed, elem) {
	var length = ruffles.length;
	ruffling = true;
	// ten second per 0.05 second speed
	var timeleft = totalTime / speed;
	var ruffleTimer = setInterval(function(){
		timeleft - --timeleft;
        var idx = timeleft % length;
        var food = ruffles[idx];
		if (pageIdx == 1 && timeleft <= switchPhase) {
			startSound.fade(1, 0.5, 500);;
			clearInterval(ruffleTimer);
			startRuffle(ruffleTime, lastPhaseTime, lowSpeed, elem);
		} else if(timeleft <= 0) {
            elem.attr("src", food.img);
			startSound.stop();
            clearInterval(ruffleTimer);
            setTimeout(function() {
                $(ruffles[idx].modal_id).modal("show");
                $('#imageRuffle').addClass("blur");
                submitPrize();
            }, 2000);
		} else {
            // elem.css("background-image", "url('"+ image+"')");
            elem.attr("src", food.img);
		}
	}, speed);
}

var submitPrize = function submitPrize() {
    // let num = parseInt(localStorage.getItem(orderNum) ? localStorage.getItem(orderNum) : 0) + 1;
    chrome.storage.sync.get("orderNum",function(obj){
        let num = parseInt(obj.orderNum ? obj.orderNum : 0) + 1;
        if(num%4 === 0 || num%5 === 0) {
            voucher = "Rp20rb";
        } else {
            voucher = "Rp15rb";
        }
        chrome.storage.sync.set({"orderNum": num},function(){
            console.log(num);
            ruffling = false;
        });
    });
    // localStorage.setItem(orderNum,  num);
}

var sound = function sound(src) {
  this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";
  document.body.appendChild(this.sound);
  this.play = function(){
    this.sound.play();
  }
  this.stop = function(){
    this.sound.pause();
  }
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};

var shuffle = function(sourceArray) {
    for (var i = 0; i < sourceArray.length - 1; i++) {
        var j = i + Math.floor(Math.random() * (sourceArray.length - i));

        var temp = sourceArray[j];
        sourceArray[j] = sourceArray[i];
        sourceArray[i] = temp;
    }
    return sourceArray;
}

